/*


Giana Yang
my1579
hw04
Dynamic memory
*/

#include <fstream>
#include <ostream>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class Warriors {
	friend ostream& operator << (ostream& os, const Warriors& warrior) {
		os <<"	"<< warrior.name << " : " << warrior.strength;
		return os;
	}
public:
	Warriors(const string& n, int stre) :name(n), strength(stre), hired(false) {}
	const string& getName()const { return name; }
	int getStrength() const { return strength; }
	bool getHire() const { return hired; }
	bool hire() {
		if (hired == false) { //if the warrior is not hired, the noble can hire him.
			hired = true;
			return false;
		}
		else {
			cerr << name << " is already hired!" << endl;
			return true;
		}
	}
	bool fire() {
		if (hired == true) { //if the warrior is hired, the noble can fire him.
			hired = false;
			return true;
		}
		else {
			cerr << name << " is already fired!" << endl;
			return false;
		}
	}

	void dead() {
		strength = 0;
	}
	void changeStrength(double harm) {
		strength = int(strength * harm);
	}
private:
	string name;
	int strength;
	bool hired;
};

class Nobles {
	friend ostream& operator << (ostream& os, const Nobles& noble) {
		os << noble.name << " has an army of " << noble.army.size() << endl;
		if (noble.army.size() != 0) {
			for (size_t i = 0; i < noble.army.size(); ++i) {
				os << *noble.army[i] << endl;
			}
		}
		else {
			os << endl;
		}
		return os;
	}
public:
	Nobles(const string& n) :name(n), dead(false) {}
	const string& getName()const { return name; }
	bool hire(Warriors& warrior) {
		if (dead == false) {
			if (warrior.getHire() != true) {
				warrior.hire();
				army.push_back(&warrior);
				
				return true;
			}
			else {
				return false;
			}
		}
		else {
			cerr << name << "can't hire because he's dead!" << endl;
			return false;
		}
	}
	void fire(Warriors& warrior) {
		if (dead != true) {
			int warriorPos = 0;
			for (size_t i = 0; i < army.size(); ++i) {
				if (army[i] == &warrior) {
					warriorPos = i;
					break;
				}
			}
			if (warriorPos != army.size() - 1) {
				for (size_t i = warriorPos; i < army.size() - 1; ++i) {
					Warriors* temp = army[warriorPos];
					army[warriorPos] = army[warriorPos + 1];
					army[warriorPos + 1] = temp;
				}
			}
			army[army.size() - 1]->fire();
			cout << army[army.size() - 1]->getName() << " You don't work for me anymore " << " -- "<<name << endl;
			army.pop_back();
		}
		else {
			cerr << name << "can't fire because he's dead!" << endl;
		}
	}
	void battle(Nobles& anotherNoble) {
		cout << name << " battles " << anotherNoble.name << endl;
		if (this->dead == true && anotherNoble.dead == true) {
			cout << "Oh, NO!  They're both dead!  Yuck!" << endl;
		}
		else if (this->dead) {
			cout << "He's dead " << anotherNoble.name << endl;
		}
		else if (anotherNoble.dead) {
			cout << "He's dead " << this->name << endl;
		}
		else {
			double mystrength = 0;
			double enemy = 0;
			for (Warriors* i : army) {
				mystrength = mystrength + i->getStrength();
			}
			for (Warriors* i : anotherNoble.army) {
				enemy = enemy + i->getStrength();
			}
			if (mystrength < enemy) {
				double ratio = 1 - (mystrength / enemy);
				for (Warriors* i : army) {
					i->dead();
					dead = true;
				}
				this->dead = true;
				for (Warriors* i : anotherNoble.army) {
					i->changeStrength(ratio);
				}
				cout << anotherNoble.name << " defeats " << name << endl;
			}
			else if (mystrength == enemy) {
				for (Warriors* i : army) {
					i->dead();
					dead = true;
				}
				this->dead = true;
				anotherNoble.dead = true;
				for (Warriors* i : anotherNoble.army) {
					i->dead();
					dead = true;
				}
				cout << "Mutual Annihalation : " << name << " and " << anotherNoble.name << " die at each other's hands" << endl;
			}
			else {//if mystrength > enemy
				double ratio = 1-( enemy / mystrength);
				for (Warriors* i : anotherNoble.army) {
					i->dead();
					dead = true;
				}
				anotherNoble.dead = true;
				for (Warriors* i : army) {
					i->changeStrength(ratio);
				}
				cout << name << " defeats " << anotherNoble.name << endl;
			}
		}
	}
private:
	string name;
	vector<Warriors*> army;
	bool dead;
};



ifstream& openFile(ifstream& ifs)
{
	ifs.open("nobleWarriors.txt");
	while (!ifs) {
		cerr << "failed to open" << endl;
		exit(1);
	}
	return ifs;
}

bool nobleExist(const string& name, const vector<Nobles*>& noble)  { //check if noble already exists
	for (size_t i = 0; i < noble.size(); ++i) {
		if (name == noble[i]->getName()) {
			cerr << "Warrior does exist" << endl;
			return true;
		}
	}
	return false;
}

bool warriorExist(const string& name, const vector<Warriors*>& warrior) { //check if warrior already exists
	for (size_t i = 0; i < warrior.size(); ++i) {
		if (name == warrior[i]->getName()) {
			cerr << "Warrior does exist" << endl;
			return true;
		}
	}
	return false;
}

void status(vector <Warriors*>& warrior, vector <Nobles*>& noble) { //print out the status
	cout << "Status" << endl << "======" << endl << "Nobles:" << endl;
	if (noble.size() == 0) {
		cout << "NONE" << endl;
	}
	else {
		for (size_t i = 0; i < noble.size(); ++i) {
			cout << *noble[i] << endl;
		}
	}
	cout << "Unemployed Warriors:" << endl;
	bool unemployed = false; // check if there are any unemployed in the heap
	for (size_t j = 0; j < warrior.size(); ++j) {
		if (warrior[j]->getHire()==false) {
			unemployed = true;
			cout << *warrior[j] << endl;
		}
	}
	if (unemployed == false) {
		cout << "NONE" << endl;
	}
}

void hire(const string& noble, const string& warrior, const vector<Nobles*>& nobles, const vector<Warriors*>& army) {
	Nobles* hirer = nullptr;
	for (size_t i = 0; i < nobles.size(); ++i) {
		if (noble == nobles[i]->getName()) {
			hirer = nobles[i];
			break;
		}
	}
	if (hirer == nullptr) {//check if noble exists
		cerr << "Noble does not exist" << endl;
		return;
	}
	Warriors* employee = nullptr;
	for (size_t i = 0; i < army.size(); ++i) {//find the warrior
		if (warrior == army[i]->getName()) {
			employee = army[i];
			break;
		}
	}
	if (employee == nullptr) {
		cerr << "Wariror does not exist" << endl;
		return;
	}
	hirer->hire(*employee);
}

void fire(const string& noble, const string& warrior, const vector<Nobles*>& nobles, const vector<Warriors*>& army) {//check and call fire function
	Nobles* firer = nullptr;
	for (size_t i = 0; i < nobles.size(); ++i) {
		if (noble == nobles[i]->getName()) {
			firer = nobles[i];
			break;
		}
	}
	if (firer == nullptr) {//check if noble exists
		cerr << "Noble does not exist" << endl;
		return;
	}
	Warriors* employee = nullptr;
	for (size_t i = 0; i < army.size(); ++i) {
		if (warrior == army[i]->getName()) {
			employee = army[i];
			break;
		}
	}
	if (employee == nullptr) {//check if warrior exist
		cerr << "Wariror does not exist" << endl;
		return;
	}
	firer->fire(*employee);
}

void Battle(const vector <Warriors*>& army, const vector <Nobles*>& nobles, const string& noble, const string& noble2) { // check and run battle
	Nobles* player = nullptr;
	Nobles* player2 = nullptr;
	for (size_t i = 0; i < nobles.size(); ++i) {
		if (noble == nobles[i]->getName()) {
			player = nobles[i];
		}
		if (noble2 == nobles[i]->getName()) {
			player2 = nobles[i];
		}
	}
	if (player == nullptr || player2 == nullptr) {
		cerr << "One of these nobles do not exist" << endl;
	}
	player->battle(*player2);
}

void clear(vector <Warriors*>& army, vector <Nobles*>& nobles) { // clear heap
	for (size_t i = 0; i < nobles.size(); ++i) {
		delete nobles[i];
	}
	nobles.clear();

	for (size_t j = 0; j < army.size(); ++j) {
		delete army[j];
	}
	army.clear();

}


void functionApply(ifstream& ifs, vector <Warriors*>& warrior, vector <Nobles*>& noble) { //read from file 
	string func;
	while (ifs >> func) {
		if (func == "Noble") {
			string name;
			if (ifs >> name) {
				if (nobleExist(name, noble) == false) {
					Nobles* newNoble = new Nobles(name);
					noble.push_back(newNoble);
				}
			}
		}
		else if (func == "Warrior") {
			string name;
			int stre;
			if (ifs >> name >> stre) {
				if (warriorExist(name, warrior) == false) {
					Warriors* newWarrior = new Warriors(name, stre);
					warrior.push_back(newWarrior);
				}
			}
		}
		else if (func == "Status") {
			status(warrior, noble);
		}
		else if (func == "Hire") {
			string name1, name2;
			if (ifs >> name1 >> name2) {
				hire(name1, name2, noble, warrior);
			}
		}
		else if (func == "Fire") {
			string name1, name2;
			if (ifs >> name1 >> name2) {
				fire(name1, name2, noble, warrior);
			}
		}
		else if (func == "Battle") {
			string name1, name2;
			if (ifs >> name1 >> name2) {
				Battle(warrior,noble,name1, name2);
			}
		}
		else { //call the clear function
			clear(warrior, noble);
		}
	}
}

int main() {
	vector <Warriors*> warrior;
	vector <Nobles*> noble;
	ifstream file;
	openFile(file);
	functionApply(file, warrior, noble);
	file.close();//close the file
}